package robfernandes.xyz.quandoocodingchallenge.util

/**
 * Created by Roberto Fernandes on 29/08/2019.
 */

const val BASE_URL = "https://api.quandoo.com/"
const val DEFAULT_CONNECTION_TIMEOUT = 60L //seconds
const val DEFAULT_MERCHANTS_CAPACITY = 2
const val DEFAULT_MERCHANTS_LIMIT = 120


