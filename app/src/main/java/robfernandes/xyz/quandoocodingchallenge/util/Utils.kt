package robfernandes.xyz.quandoocodingchallenge.util

import android.widget.ImageView
import com.squareup.picasso.Picasso

/**
 * Created by Roberto Fernandes on 30/08/2019.
 */

fun loadUrlIntoImageView (imageView: ImageView, url: String ) =
    Picasso.get()
        .load(url)
        .fit()
        .centerCrop()
        .into(imageView)