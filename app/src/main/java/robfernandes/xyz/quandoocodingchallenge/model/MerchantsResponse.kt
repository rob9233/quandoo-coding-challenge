package robfernandes.xyz.quandoocodingchallenge.model


import com.google.gson.annotations.SerializedName

data class MerchantsResponse(
    @SerializedName("limit")
    val limit: Int?,
    @SerializedName("merchants")
    val merchants: List<Merchant?>?,
    @SerializedName("offset")
    val offset: Int?,
    @SerializedName("size")
    val size: Int?
) {
    data class Merchant(
        @SerializedName("bookable")
        val bookable: Boolean?,
        @SerializedName("ccvEnabled")
        val ccvEnabled: Boolean?,
        @SerializedName("chain")
        val chain: Chain?,
        @SerializedName("currency")
        val currency: String?,
        @SerializedName("documents")
        val documents: List<Any?>?,
        @SerializedName("id")
        val id: Int?,
        @SerializedName("images")
        val images: List<Image?>?,
        @SerializedName("links")
        val links: List<Link?>?,
        @SerializedName("locale")
        val locale: String?,
        @SerializedName("location")
        val location: Location?,
        @SerializedName("name")
        val name: String?,
        @SerializedName("openingTimes")
        val openingTimes: OpeningTimes?,
        @SerializedName("phoneNumber")
        val phoneNumber: String?,
        @SerializedName("reviewScore")
        val reviewScore: String?,
        @SerializedName("tagGroups")
        val tagGroups: List<TagGroup?>?,
        @SerializedName("timezone")
        val timezone: String?
    ) {
        data class Chain(
            @SerializedName("id")
            val id: Int?,
            @SerializedName("name")
            val name: String?
        )

        data class OpeningTimes(
            @SerializedName("standardOpeningTimes")
            val standardOpeningTimes: StandardOpeningTimes?
        ) {
            data class StandardOpeningTimes(
                @SerializedName("SATURDAY")
                val sATURDAY: List<SATURDAY?>?,
                @SerializedName("SUNDAY")
                val sUNDAY: List<SUNDAY?>?
            ) {
                data class SATURDAY(
                    @SerializedName("end")
                    val end: String?,
                    @SerializedName("start")
                    val start: String?
                )

                data class SUNDAY(
                    @SerializedName("end")
                    val end: String?,
                    @SerializedName("start")
                    val start: String?
                )
            }
        }

        data class Image(
            @SerializedName("url")
            val url: String?
        )

        data class Location(
            @SerializedName("address")
            val address: Address?,
            @SerializedName("coordinates")
            val coordinates: Coordinates?
        ) {
            data class Address(
                @SerializedName("city")
                val city: String?,
                @SerializedName("country")
                val country: String?,
                @SerializedName("number")
                val number: String?,
                @SerializedName("street")
                val street: String?,
                @SerializedName("zipcode")
                val zipcode: String?
            )

            data class Coordinates(
                @SerializedName("latitude")
                val latitude: Double?,
                @SerializedName("longitude")
                val longitude: Double?
            )
        }

        data class Link(
            @SerializedName("href")
            val href: String?,
            @SerializedName("method")
            val method: String?,
            @SerializedName("rel")
            val rel: String?
        )

        data class TagGroup(
            @SerializedName("tags")
            val tags: List<Tag?>?,
            @SerializedName("type")
            val type: String?
        ) {
            data class Tag(
                @SerializedName("id")
                val id: String?,
                @SerializedName("name")
                val name: String?
            )
        }
    }
}