package robfernandes.xyz.quandoocodingchallenge.dataSource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import io.reactivex.disposables.CompositeDisposable
import robfernandes.xyz.quandoocodingchallenge.model.MerchantsResponse.Merchant
import robfernandes.xyz.quandoocodingchallenge.network.QuandooApiInterface

/**
 * Created by Roberto Fernandes on 29/08/2019.
 */

class MerchantDataSourceFactory(
    private val apiService: QuandooApiInterface,
    private val compositeDisposable: CompositeDisposable
) : DataSource.Factory<Int, Merchant>() {

    val merchantLiveDataSource = MutableLiveData<MerchantDataSource>()

    override fun create(): DataSource<Int, Merchant> {
        val merchantDataSource =
            MerchantDataSource(
                apiService,
                compositeDisposable
            )

        merchantLiveDataSource.postValue(merchantDataSource)
        return merchantDataSource
    }
}