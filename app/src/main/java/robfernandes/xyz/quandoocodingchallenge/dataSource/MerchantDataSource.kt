package robfernandes.xyz.quandoocodingchallenge.dataSource

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import robfernandes.xyz.quandoocodingchallenge.model.MerchantsResponse.Merchant
import robfernandes.xyz.quandoocodingchallenge.network.NetworkState
import robfernandes.xyz.quandoocodingchallenge.network.QuandooApiInterface
import robfernandes.xyz.quandoocodingchallenge.util.DEFAULT_MERCHANTS_CAPACITY
import robfernandes.xyz.quandoocodingchallenge.util.DEFAULT_MERCHANTS_LIMIT

/**
 * Created by Roberto Fernandes on 29/08/2019.
 */

class MerchantDataSource(
    private val apiService: QuandooApiInterface
    , private val compositeDisposable: CompositeDisposable
) : PageKeyedDataSource<Int, Merchant>() {

    private var offset = 0

    val networkState: MutableLiveData<NetworkState> = MutableLiveData()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Merchant>
    ) {
        networkState.postValue(NetworkState.LOADING)

        compositeDisposable.add(
            apiService.getMerchants(
                DEFAULT_MERCHANTS_CAPACITY, offset,
                DEFAULT_MERCHANTS_LIMIT
            )
                .subscribeOn(Schedulers.io())
                .subscribe(
                    {
                        it.merchants?.let { merchants ->
                            callback.onResult(
                                merchants, null, offset
                                        + DEFAULT_MERCHANTS_LIMIT
                            )
                        }
                        networkState.postValue(NetworkState.LOADED)
                    },
                    {
                        networkState.postValue(NetworkState.ERROR)
                        Log.e("MerchantDataSource", it.message)
                    }
                )
        )
    }

    override fun loadAfter(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, Merchant>
    ) {
        networkState.postValue(NetworkState.LOADING)

        compositeDisposable.add(
            apiService.getMerchants(
                DEFAULT_MERCHANTS_CAPACITY, params.key,
                DEFAULT_MERCHANTS_LIMIT
            )
                .subscribeOn(Schedulers.io())
                .subscribe(
                    {
                        //return to the first item when the list reaches the end
                        if (
                            offset > 0
                            && (it.merchants == null || it.merchants.size < DEFAULT_MERCHANTS_LIMIT)
                        ) {
                            offset = 0
                        }


                        it.merchants?.let { merchants ->
                            callback.onResult(
                                merchants, params.key + DEFAULT_MERCHANTS_LIMIT
                            )
                        }
                        networkState.postValue(NetworkState.LOADED)

                    },
                    {
                        networkState.postValue(NetworkState.ERROR)
                        Log.e("MerchantDataSource", it.message)
                    }
                )
        )
    }

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, Merchant>
    ) {
    }
}