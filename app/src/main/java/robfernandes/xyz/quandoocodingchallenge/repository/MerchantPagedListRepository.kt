package robfernandes.xyz.quandoocodingchallenge.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import io.reactivex.disposables.CompositeDisposable
import robfernandes.xyz.quandoocodingchallenge.dataSource.MerchantDataSource
import robfernandes.xyz.quandoocodingchallenge.dataSource.MerchantDataSourceFactory
import robfernandes.xyz.quandoocodingchallenge.model.MerchantsResponse.Merchant
import robfernandes.xyz.quandoocodingchallenge.network.NetworkState
import robfernandes.xyz.quandoocodingchallenge.network.QuandooApiInterface

/**
 * Created by Roberto Fernandes on 29/08/2019.
 */

class MerchantPagedListRepository(private val apiService: QuandooApiInterface) {

    val pageSize = 30

    lateinit var merchantPagedList: LiveData<PagedList<Merchant>>
    lateinit var merchantDataSourceFactory: MerchantDataSourceFactory

    fun fetchLiveMoviePagedList(compositeDisposable: CompositeDisposable)
            : LiveData<PagedList<Merchant>> {
        merchantDataSourceFactory =
            MerchantDataSourceFactory(
                apiService,
                compositeDisposable
            )

        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(pageSize)
            .build()

        merchantPagedList = LivePagedListBuilder(merchantDataSourceFactory, config).build()

        return merchantPagedList
    }

    fun getNetworkState(): LiveData<NetworkState> {
        return Transformations.switchMap<MerchantDataSource, NetworkState>(
            merchantDataSourceFactory.merchantLiveDataSource, MerchantDataSource::networkState
        )
    }
}