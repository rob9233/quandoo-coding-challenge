package robfernandes.xyz.quandoocodingchallenge.network

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import robfernandes.xyz.quandoocodingchallenge.model.MerchantsResponse

/**
 * Created by Roberto Fernandes on 29/08/2019.
 */

interface QuandooApiInterface {
    @GET("v1/merchants")
    fun getMerchants(
        @Query("capacity") capacity: Int
        , @Query("offset") offset: Int
        , @Query("limit") limit: Int
    ): Single<MerchantsResponse>
}