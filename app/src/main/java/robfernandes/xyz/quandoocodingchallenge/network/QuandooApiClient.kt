package robfernandes.xyz.quandoocodingchallenge.network

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import robfernandes.xyz.quandoocodingchallenge.util.BASE_URL
import robfernandes.xyz.quandoocodingchallenge.util.DEFAULT_CONNECTION_TIMEOUT
import java.util.concurrent.TimeUnit

/**
 * Created by Roberto Fernandes on 29/08/2019.
 */
object QuandooApiClient {

    fun getClient(): QuandooApiInterface {

        val okHttpClient = OkHttpClient.Builder()
            .connectTimeout(DEFAULT_CONNECTION_TIMEOUT, TimeUnit.SECONDS)
            .build()

        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(QuandooApiInterface::class.java)
    }
}