package robfernandes.xyz.quandoocodingchallenge.adapter

import robfernandes.xyz.quandoocodingchallenge.model.MerchantsResponse

/**
 * Created by Roberto Fernandes on 30/08/2019.
 */
interface MerchantClickListener {
    fun onMerchantClicked(merchant: MerchantsResponse.Merchant)
}