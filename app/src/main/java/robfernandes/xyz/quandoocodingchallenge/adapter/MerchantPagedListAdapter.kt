package robfernandes.xyz.quandoocodingchallenge.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.merchant_list_item.view.*
import kotlinx.android.synthetic.main.network_state_item.view.*
import robfernandes.xyz.quandoocodingchallenge.R
import robfernandes.xyz.quandoocodingchallenge.model.MerchantsResponse.Merchant
import robfernandes.xyz.quandoocodingchallenge.network.NetworkState
import robfernandes.xyz.quandoocodingchallenge.util.loadUrlIntoImageView

/**
 * Created by Roberto Fernandes on 29/08/2019.
 */

class MerchantPagedListAdapter(val context: Context) :
    PagedListAdapter<Merchant, RecyclerView.ViewHolder>(MerchantDiffCallback()) {

    val MERCHANT_VIEW_TYPE = 1
    val NETWORK_VIEW_TYPE = 2

    var merchantClickListener: MerchantClickListener? = null
    private var networkState: NetworkState? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View

        return if (viewType == MERCHANT_VIEW_TYPE) {
            view = layoutInflater.inflate(R.layout.merchant_list_item, parent, false)
            MovieItemViewHolder(
                view
            )
        } else {
            view = layoutInflater.inflate(R.layout.network_state_item, parent, false)
            NetworkStateItemViewHolder(
                view
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == MERCHANT_VIEW_TYPE) {
            getItem(position)?.let { (holder as MovieItemViewHolder).bind(it, context) }
        } else {
            (holder as NetworkStateItemViewHolder).bind(networkState)
        }
    }

    class MerchantDiffCallback : DiffUtil.ItemCallback<Merchant>() {
        override fun areItemsTheSame(oldItem: Merchant, newItem: Merchant): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Merchant, newItem: Merchant): Boolean {
            return oldItem == newItem
        }
    }

    fun setOnMerchantClickListener(listener: MerchantClickListener) {
        this.merchantClickListener = listener
    }

    inner class MovieItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(merchant: Merchant, context: Context) {
            itemView.merchant_list_item.text = merchant.name

            val merchantImageURL = merchant.images?.get(0)?.url

            merchantImageURL?.let {
                loadUrlIntoImageView(itemView.merchant_list_item_image,
                    it
                )
            }

            itemView.setOnClickListener {
                merchantClickListener?.onMerchantClicked(merchant)
            }
        }
    }

    class NetworkStateItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(networkState: NetworkState?) {
            if (networkState != null && networkState == NetworkState.LOADING) {
                itemView.error_msg_item_progress_bar_item.visibility = View.VISIBLE
            } else {
                itemView.error_msg_item_progress_bar_item.visibility = View.GONE
            }

            if (networkState != null && networkState == NetworkState.ERROR) {
                itemView.error_msg_item_error_msg_item.visibility = View.VISIBLE
                itemView.error_msg_item_error_msg_item.text = networkState.msg
            } else {
                itemView.error_msg_item_error_msg_item.visibility = View.GONE
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow() && position == itemCount - 1) {
            NETWORK_VIEW_TYPE
        } else {
            MERCHANT_VIEW_TYPE
        }
    }

    fun setNetworkState(newNetworkState: NetworkState) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()

        //logic to add/remove progress bar at the end
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                notifyItemRemoved(super.getItemCount())
            } else {
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && previousState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }

    }

    private fun hasExtraRow(): Boolean {
        return networkState != null && networkState != NetworkState.LOADED
    }
}