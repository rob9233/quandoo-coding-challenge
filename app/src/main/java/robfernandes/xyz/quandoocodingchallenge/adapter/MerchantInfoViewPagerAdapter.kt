package robfernandes.xyz.quandoocodingchallenge.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import robfernandes.xyz.quandoocodingchallenge.util.loadUrlIntoImageView

/**
 * Created by Roberto Fernandes on 30/08/2019.
 */

class MerchantInfoViewPagerAdapter(val context: Context, val urls: ArrayList<String>) :
    PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean = view === `object`

    override fun getCount(): Int = urls.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imageView = ImageView(context)

        loadUrlIntoImageView(imageView, urls[position])
        container.addView(imageView)

        return imageView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}