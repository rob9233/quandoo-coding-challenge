package robfernandes.xyz.quandoocodingchallenge.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*
import robfernandes.xyz.quandoocodingchallenge.R
import robfernandes.xyz.quandoocodingchallenge.adapter.MerchantClickListener
import robfernandes.xyz.quandoocodingchallenge.adapter.MerchantPagedListAdapter
import robfernandes.xyz.quandoocodingchallenge.model.MerchantsResponse
import robfernandes.xyz.quandoocodingchallenge.network.NetworkState
import robfernandes.xyz.quandoocodingchallenge.viewModel.MainActivityViewModel
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainActivityViewModel
    private lateinit var merchantAdapter: MerchantPagedListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setToolbar()
        viewModel = getViewModel()
        setMerchantPagedListAdapter()
        setObservers()
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
    }

    private fun getViewModel(): MainActivityViewModel {
        return ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
    }

    private fun setMerchantPagedListAdapter() {
        val gridLayoutManager = GridLayoutManager(this, 2)
        merchantAdapter = MerchantPagedListAdapter(this)

        merchantAdapter.setOnMerchantClickListener(object : MerchantClickListener {
            override fun onMerchantClicked(merchant: MerchantsResponse.Merchant) {
                goToMerchantInfoActivity(merchant)
            }
        })

        activity_main_recycler_view.layoutManager = gridLayoutManager
        activity_main_recycler_view.setHasFixedSize(true)
        activity_main_recycler_view.adapter = merchantAdapter
    }

    private fun setObservers() {
        viewModel.merchantPagedList.observe(this, Observer {
            merchantAdapter.submitList(it)
        })

        viewModel.networkState.observe(this, Observer {
            activity_main_progress_bar_error.visibility =
                if (viewModel.listIsEmpty() && it == NetworkState.LOADING) View.VISIBLE
                else View.GONE
            activity_main_error_text_view.visibility =
                if (viewModel.listIsEmpty() && it == NetworkState.ERROR) View.VISIBLE
                else View.GONE

            if (!viewModel.listIsEmpty()) merchantAdapter.setNetworkState(it)
        })
    }

    private fun goToMerchantInfoActivity(merchant: MerchantsResponse.Merchant) {
        val intent = Intent(this@MainActivity, MerchantInfoActivity::class.java)

        val imagesUrl = mutableListOf<String>()

        merchant.images?.forEach {
            it?.url?.let { it1 -> imagesUrl.add(it1) }
        }
        val address = merchant.location?.address

        intent.putExtra(BUNDLE_KEY_NAME, merchant.name)
        intent.putExtra(BUNDLE_KEY_IMAGES, ArrayList(imagesUrl))
        intent.putExtra(BUNDLE_KEY_CITY, address?.city)
        intent.putExtra(BUNDLE_KEY_COUNTRY, address?.country)
        intent.putExtra(BUNDLE_KEY_STREET, address?.street)
        intent.putExtra(BUNDLE_KEY_ZIPCODE, address?.zipcode)
        intent.putExtra(BUNDLE_KEY_RATING, merchant.reviewScore)
        intent.putExtra(BUNDLE_KEY_PHONE, merchant.phoneNumber)
        startActivity(intent)
    }

    companion object BundleKeys {
        const val BUNDLE_KEY_NAME = "BUNDLE_KEY_NAME"
        const val BUNDLE_KEY_IMAGES = "BUNDLE_KEY_IMAGES"
        const val BUNDLE_KEY_CITY = "BUNDLE_KEY_CITY"
        const val BUNDLE_KEY_COUNTRY = "BUNDLE_KEY_COUNTRY"
        const val BUNDLE_KEY_STREET = "BUNDLE_KEY_STREET"
        const val BUNDLE_KEY_ZIPCODE = "BUNDLE_KEY_ZIPCODE"
        const val BUNDLE_KEY_RATING = "BUNDLE_KEY_RATING"
        const val BUNDLE_KEY_PHONE = "BUNDLE_KEY_PHONE"
    }
}
