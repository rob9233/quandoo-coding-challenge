package robfernandes.xyz.quandoocodingchallenge.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_merchant_info.*
import kotlinx.android.synthetic.main.toolbar.*
import robfernandes.xyz.quandoocodingchallenge.R
import robfernandes.xyz.quandoocodingchallenge.adapter.MerchantInfoViewPagerAdapter
import robfernandes.xyz.quandoocodingchallenge.viewModel.MerchantInfoActivityViewModel

class MerchantInfoActivity : AppCompatActivity() {

    private lateinit var viewModel: MerchantInfoActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_merchant_info)

        setToolbar()
        viewModel = getViewModel()
        setObservables()
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.restaurant_details)
    }

    private fun getViewModel(): MerchantInfoActivityViewModel {
        val merchantInfoActivityViewModel =
            ViewModelProviders.of(this, object : ViewModelProvider.Factory {
                override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                    @Suppress("UNCHECKED_CAST")
                    return MerchantInfoActivityViewModel(intent) as T
                }
            })[MerchantInfoActivityViewModel::class.java]

        merchantInfoActivityViewModel.setValues()

        return merchantInfoActivityViewModel
    }

    private fun setObservables() {
        viewModel.name.observe(this, Observer<String> {
            activity_merchant_info_name.text = it
        })
        viewModel.imagesUrls.observe(this, Observer<ArrayList<String>> {
            setViewPagerAdapter(it)
        })
        viewModel.rating.observe(this, Observer<String> {
            val text = "${getString(R.string.rating)} $it"
            activity_merchant_info_rating.text = text
        })
        viewModel.phone.observe(this, Observer<String> {
            val text = "${getString(R.string.phone)} $it"
            activity_merchant_info_phone.text = text
        })
        viewModel.address.observe(this, Observer<String> {
            val text = "${getString(R.string.address)} $it"
            activity_merchant_info_address.text = text
        })
    }

    private fun setViewPagerAdapter(urls: ArrayList<String>) {
        val viewPagerAdapter = MerchantInfoViewPagerAdapter(baseContext, urls)
        activity_merchant_images_view_pager.adapter = viewPagerAdapter
        activity_merchant_images_tab_dots.setupWithViewPager(
            activity_merchant_images_view_pager, true
        )
    }
}
