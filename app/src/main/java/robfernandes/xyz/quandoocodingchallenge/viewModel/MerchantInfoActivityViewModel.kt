package robfernandes.xyz.quandoocodingchallenge.viewModel

import android.content.Intent
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import robfernandes.xyz.quandoocodingchallenge.ui.activities.MainActivity

/**
 * Created by Roberto Fernandes on 30/08/2019.
 */

class MerchantInfoActivityViewModel(private val intent: Intent) : ViewModel() {

    val name = MutableLiveData<String>()
    val imagesUrls = MutableLiveData<ArrayList<String>>()
    val rating = MutableLiveData<String>()
    val address = MutableLiveData<String>()
    val phone = MutableLiveData<String>()

    fun setValues() {
        name.postValue(intent.getStringExtra(MainActivity.BUNDLE_KEY_NAME))
        rating.postValue(intent.getStringExtra(MainActivity.BUNDLE_KEY_RATING))
        phone.postValue(intent.getStringExtra(MainActivity.BUNDLE_KEY_PHONE))

        val city = intent.getStringExtra(MainActivity.BUNDLE_KEY_CITY)
        val country = intent.getStringExtra(MainActivity.BUNDLE_KEY_COUNTRY)
        val street = intent.getStringExtra(MainActivity.BUNDLE_KEY_STREET)
        val zipCode = intent.getStringExtra(MainActivity.BUNDLE_KEY_ZIPCODE)

        val addressString = "\n$street\n$zipCode\n$city, $country"

        address.postValue(addressString)

        @Suppress("UNCHECKED_CAST")
        imagesUrls.postValue(
            intent.getSerializableExtra(MainActivity.BUNDLE_KEY_IMAGES) as ArrayList<String>
        )
    }
}