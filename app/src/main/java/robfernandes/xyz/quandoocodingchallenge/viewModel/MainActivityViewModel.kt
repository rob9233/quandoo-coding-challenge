package robfernandes.xyz.quandoocodingchallenge.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import io.reactivex.disposables.CompositeDisposable
import robfernandes.xyz.quandoocodingchallenge.model.MerchantsResponse.Merchant
import robfernandes.xyz.quandoocodingchallenge.network.NetworkState
import robfernandes.xyz.quandoocodingchallenge.network.QuandooApiClient
import robfernandes.xyz.quandoocodingchallenge.network.QuandooApiInterface
import robfernandes.xyz.quandoocodingchallenge.repository.MerchantPagedListRepository

/**
 * Created by Roberto Fernandes on 29/08/2019.
 */

class MainActivityViewModel : ViewModel() {

    val apiService: QuandooApiInterface = QuandooApiClient.getClient()

    val merchantRepository = MerchantPagedListRepository(apiService)

    val compositeDisposable = CompositeDisposable()

    val merchantPagedList: LiveData<PagedList<Merchant>> by lazy {
        merchantRepository.fetchLiveMoviePagedList(compositeDisposable)
    }

    val networkState: LiveData<NetworkState> by lazy {
        merchantRepository.getNetworkState()
    }

    fun listIsEmpty(): Boolean {
        return merchantPagedList.value?.isEmpty() ?: true
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}