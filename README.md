# Quandoo's technical assessment

When the app is launched it fetchs the first 120 merchants (with a 30 items/page limit).
When the user taps on the merchant , it leads to a merchant detail page

## Screenshots

<img src="https://i.ibb.co/2qwqs6J/Screenshot-20190831-114336-robfernandes-xyz-quandoocodingchallenge.jpg"  height="400" border="0">

<img src="https://i.ibb.co/x3NRfP3/Screenshot-20190831-114413-robfernandes-xyz-quandoocodingchallenge.jpg"  height="400" border="0">

## It uses

- Kotlin
- MVVM architectural pattern
- Paging library for Pagination
- RxJava
- Retrofit
- Quandoo's test public API